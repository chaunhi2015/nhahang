<?php
/**
 * Template Name: Booked
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 ?>
 
 <?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea order container">
					<?php
						if(have_posts()):while(have_posts()):the_post();
						$data = get_post_meta( get_the_ID(), 'wpcf-choose-order', false );
						$foodtour = '';
						foreach($data as $val){															
							$foodtour = $val;
						}
					?>
					<div class="parsys_column booking-order <?php if($foodtour == 2) echo "foodtour";?>">
						<div class="top_page"></div>
						<div class="bottom_page"></div>
						<div class="content_wapper">
							<div class="content">
								<?php							
									echo "<h2 class='title'>".get_the_title()."</h2>";
									the_content();
								?>
									<div class="texts">
										<?php
											$data = get_post_meta( get_the_ID(), 'wpcf-gia', false );								
											foreach($data as $val){															
												echo $val;
											}
										?>
									</div>
									<div class="price-text">
										PRICES ARE ALL INCLUSIVE
									</div>
									<?php
										$link = '';
										if($foodtour == 1){
											$link = get_site_url().'/order-menu';
										}else{
											$link = get_site_url().'/foodtour-order';
										}
									?>
									<form action="<?php echo $link?>" method="POST">
										<input type="hidden" name="title_name" value="<?php echo get_the_title();?>"/>
										<input type="submit" class="btn btn-outline btn-warning fl_right" value="Book Now"/>
									</form>								
								<p>

								<div class="col-md-9 col-xs-12 col-sm-9 col-md-push-2 col-sm-push-2">
									<div id="owl-book" class="owl-carousel"> 
										<?php
											$image_list = '';
											$attachment_ids = easy_image_gallery_get_image_ids();											
											foreach ( $attachment_ids as $attachment_id ) {
												$image_link	= wp_get_attachment_image_src( $attachment_id, apply_filters( 'easy_image_gallery_linked_image_size', 'large' ) );
												$image	= $image_link[0];										
												$image_list .= '<div class="item">';
													$image_list .= '<img src="'.$image.'" alt="'.get_the_title().'">';
												$image_list .= '</div>';
											}
																				
											echo $image_list;
										?>								
									</div>
									<script>
										jQuery(window).ready(function () {				
											var sowl = jQuery('#owl-book');
											sowl.owlCarousel({
												itemsCustom : [
													[0, 1],
													[450, 1],
													[600, 1],
													[700, 1],
													[800, 1],
													[1000, 1],
													[1200, 1],
													[1400, 1],
													[1600, 1]
												],
												navigation : false,
												pagination:false,
												autoPlay:15000,
												slideSpeed:10000,
											});		
										});
									</script>
								</div>						
								<!--<img src="<?php echo bloginfo("stylesheet_directory");?>/images/img_slider.jpg"/>--> </p>
							</div>	
						</div>	
					</div>	
					<?php
						endwhile;endif;wp_reset_query();
					?>
				</div>
			</div>
		<!--Content Area Ends -->
	<?php get_footer(); ?>