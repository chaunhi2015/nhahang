﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div class="master-wrapper-main">
            <div class="master-wrapper-content">
                


<div class="center-2">
    <div class="breadcrumb">
        <?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
    </div>
	<div class="page search-page">
		<div class="page-title">
			<h1><?php single_cat_title();?></h1>
		</div>
		<div class="page-body">
			<div class="search-input">
				<?php if(function_exists('wp_custom_fields_search')) 
							wp_custom_fields_search(); ?>				
			</div>
			

				<div class="category-description">
					<?php echo category_description(); ?>
				</div>			
			<div style="border-bottom:1px solid #ebebeb;margin-bottom:20px"></div>
			<div class="item-selectors">
					<?php
						global $wp;
						$current_url =  home_url( $wp->request ) ;			
						$thiscat =  get_query_var('cat'); 
					?>
					<div class="item-sorting">
						<span>Sort by</span>
						<select id="item-orderby" name="item-orderby" onchange="setLocation(this.value);">
							<option selected="selected" value="<?php echo $current_url."?created=desc" ?>">Position</option>
							<option value="<?php echo $current_url."?title=asc" ?>">Name: A to Z</option>
							<option value="<?php echo $current_url."?title=desc" ?>">Name: Z to A</option>
							<option value="<?php echo $current_url."?price=asc" ?>">Price: Low to High</option>
							<option value="<?php echo $current_url."?price=desc" ?>">Price: High to Low</option>
							<option value="<?php echo $current_url."?created=desc" ?>">Created on</option>
						</select>
					</div>
					<div class="item-page-size">
						<span>Display</span>
						<select id="item-pagesize" name="item-pagesize" onchange="setLocation(this.value);">
							<option selected="selected" value="<?php echo $current_url."?pagesize=8" ?>">8</option>
							<option value="<?php echo $current_url."?pagesize=12" ?>">12</option>
						</select>
						<span>per page</span>
					</div>
			</div>
			
			
			
				<div class="tour-list">					
					<?php															
						if(have_posts()):while(have_posts()):the_post();
					?>
						<div class="item-box">                
							<div class="tour-item">
								<div class="picture">
									<a href="<?php the_permalink();?>" title="<?php the_title();?>">
										<?php the_post_thumbnail('full');?>
									</a>
								</div>
								<div class="details">
									<h2 class="tour-title">
										<a href="<?php the_permalink();?>"><?php the_title();?></a>
									</h2>
									<?php 		
										$duration = '';
										$data = get_post_meta( get_the_ID(), 'wpcf-duration', false );								
										foreach($data as $val){															
											$duration = $val;
										} 
									?>
										<?php if($duration){ ?>
											<div class="duration">									
												<span class="label">Duration</span>: <span class="value" itemprop="duration"><?php echo $duration;?></span>
											</div>
										<?php } ?>
										<?php 		
											$destinations = '';
											$data = get_post_meta( get_the_ID(), 'wpcf-destinations', false );								
											foreach($data as $val){															
												$destinations = $val;
											} 
										?>
										<?php if($destinations){ ?>
											<div class="destinations">
												<span class="label">Destinations</span>: <span class="value" itemprop="destiantions"><?php echo $destinations;?></span>
											</div>
										<?php } ?>
									<div class="description">
										<?php echo get_the_excerpt();?>
									</div>
									<div class="add-info">
										<?php 		
											$price_vnd = '';
											$price_usd = '';
											$data = get_post_meta( get_the_ID(), 'wpcf-price-vnd', false );								
											foreach($data as $val){															
												$price_vnd = $val;
											}
											$data = get_post_meta( get_the_ID(), 'wpcf-price-usd', false );								
											foreach($data as $val){															
												$price_usd = $val;
											}
										?>
										<?php if($price_vnd){ ?>
											<div class="prices">
												<span class="pricerange">From:</span>
												<span class="price actual-price"><?php echo $price_vnd;?> VND</span> <span class="pricerange">equals</span> <span class="price actual-price"><?php echo $price_usd;?> USD</span>
											</div>
										<?php }else{
										?>
											<div class="prices">
												<span class="pricerange">From:</span>
												<span class="price actual-price"></span> <span class="pricerange">equals</span> <span class="price actual-price">Call for pricing</span>
											</div>
										<?php
										} ?>
										<div class="buttons">
											<input type="button" value="Details" class="detail-button1" onclick="setLocation('<?php the_permalink();?>')" />									
										</div>
										
									</div>
								</div>
							</div>
						</div>
					<?php
							endwhile;endif;wp_reset_query();
						?>
				</div>       
			<div class="pager">
				
			</div>
			
		</div>
	</div>

    
</div>
<div class="side-2">


   <?php
	genesis_after_content_sidebar_wrap();
   ?>            

</div>
            </div>
        
    </div>	
	<?php get_footer(); ?>