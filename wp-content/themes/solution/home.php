<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

<?php genesis_before_content(); ?>

    <!--Content Area starts -->
    <div class="content_box clearfix">
        <div class="parsys contentarea">
            <div style="clear:both"></div>
            <div class="parsys_column">
                <div class="container">
                    <div class="row">
                        <?php
                        query_posts(array('post_parent' => 4, "post_type" => "page", "posts_per_page" => 3));
                        if (have_posts()):while (have_posts()):the_post();
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                                <div class="thumbnail">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(array(315, 250), array('class' => 'image-responsive-c')); ?>
                                    </a>
                                </div>
                                <div class="caption">
                                    <div class="titlecomponent section">
                                        <h2 class="titlenone"><?php the_title(); ?></h2>
                                    </div>

                                    <p class="text">
                                        <?php echo substr(get_the_excerpt(), 0, 90); ?>
                                    </p>


                                    <p class="textlink">
                                        <?php
                                        $data = get_post_meta(get_the_ID(), 'wpcf-choose-order', false);
                                        $foodtour = '';
                                        foreach ($data as $val) {
                                            $foodtour = $val;
                                        }
                                        
										$link = '';
										if($foodtour == 1){
											$link = get_site_url().'/order-menu';
										}else{
											$link = get_site_url().'/foodtour-order';
										}
										?>
										<form class="text-center" action="<?php echo $link?>" method="POST">
											<input type="hidden" name="title_name" value="<?php echo get_the_title();?>"/>
											<input type="submit" class="btn btn-outline btn-warning fl_right" value="Book Now"/>
										</form>
                                    </p>
                                </div>
                            </div>
                        <?php endwhile;endif;
                        wp_reset_query(); ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <?php
                        query_posts(array('post_parent' => 21, "post_type" => "page", "posts_per_page" => 3));
                        if (have_posts()):while (have_posts()):the_post();
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">


                                <div class="thumbnail">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(array(315, 250), array('class' => 'image-responsive-c')); ?>
                                    </a>
                                </div>
                                <div class="caption">
                                    <div class="titlecomponent section">
                                        <h2 class="titlenone"><?php the_title(); ?></h2>
                                    </div>

                                    <p class="text">
                                        <?php echo substr(get_the_excerpt(), 0, 90); ?>
                                    </p>


                                    <p class="textlink">
                                        <?php
                                        $data = get_post_meta(get_the_ID(), 'wpcf-choose-order', false);
                                        $foodtour = '';
                                        foreach ($data as $val) {
                                            $foodtour = $val;
                                        }
                                        $link = '';
										if($foodtour == 1){
											$link = get_site_url().'/order-menu';
										}else{
											$link = get_site_url().'/foodtour-order';
										}
										?>
										<form class="text-center" action="<?php echo $link?>" method="POST">
											<input type="hidden" name="title_name" value="<?php echo get_the_title();?>"/>
											<input type="submit" class="btn btn-outline btn-warning fl_right" value="Book Now"/>
										</form>
                                    </p>
                                </div>
                            </div>
                        <?php endwhile;endif;
                        wp_reset_query(); ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <?php
                        query_posts(array('post_parent' => 13, "post_type" => "page", "posts_per_page" => 3));
                        if (have_posts()):while (have_posts()):the_post();
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                                <div class="thumbnail">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(array(315, 250), array('class' => 'image-responsive-c')); ?>
                                    </a>
                                </div>
                                <div class="caption">
                                    <div class="titlecomponent section">
                                        <h2 class="titlenone"><?php the_title(); ?></h2>
                                    </div>

                                    <p class="text">
                                        <?php echo substr(get_the_excerpt(), 0, 90); ?>
                                    </p>


                                    <p class="textlink">
                                        <?php
                                        $data = get_post_meta(get_the_ID(), 'wpcf-choose-order', false);
                                        $foodtour = '';
                                        foreach ($data as $val) {
                                            $foodtour = $val;
                                        }
                                        $link = '';
										if($foodtour == 1){
											$link = get_site_url().'/order-menu';
										}else{
											$link = get_site_url().'/foodtour-order';
										}
										?>
										<form class="text-center" action="<?php echo $link?>" method="POST">
											<input type="hidden" name="title_name" value="<?php echo get_the_title();?>"/>
											<input type="submit" class="btn btn-outline btn-warning fl_right" value="Book Now"/>
										</form>
                                    </p>
                                </div>
                            </div>
                        <?php endwhile;
                        endif;
                        wp_reset_query(); ?>
                    </div>
                </div>
            </div>

            <div style="clear:both"></div>
            <div class="container">
                <div class="row">
                    <ul class="">
                        <?php
						function linkExtractor($html){
							 $linkArray = array();
							 if(preg_match_all('/<img\s+.*?src=[\"\']?([^\"\' >]*)[\"\']?[^>]*>/i',$html,$matches,PREG_SET_ORDER)){
							  foreach($matches as $match){
							   array_push($linkArray,array($match[1],$match[2]));
							  }
							 }
							 return $linkArray[0][0];
						}
                        query_posts(array('cat' => 13, "posts_per_page" => 4));
                        if (have_posts()):while (have_posts()):the_post();
                            ?>
                            <li class="testimonial-items bx-clone col-sm-5 col-md-5 col-lg-5 ">
                                <div class="item">
                                    <div class="picture">
                                        <a href="<?php the_permalink();?>" title="">
                                            <?php $avatar = get_avatar($post->post_author, 120);
												if (strpos($avatar, 'uploads') !== false) {														
													$link = linkExtractor($avatar);	
													$avatar = get_site_url()."/wp-content".$link;
													echo '<img src="'.$avatar.'" width="120" height="120" />';
												}else{
													echo $avatar;
												}												
											?>
											
                                        </a>
                                    </div>
									<div class="title">
                                        <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                                    </div>
                                    <div class="testimonial-details">
                                        <?php echo substr(get_the_content(),0,160); ?>
                                    </div>
                                    <div class="guest-name">
                                        - <?php echo the_author_meta( 'user_nicename', $post->post_author ); ?> -
                                    </div>
                                </div>
                            </li>
                        <?php endwhile;
                        endif;
                        wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Content Area Ends -->


<?php get_footer(); ?>