</div>
</div>
<!-- Footer Starts -->
<div class="head_foot_bg">
    <div id="footer" class="global_width">
        <div class="clearfix" id="local_footer">
            <div id="ja-botsl1" class="wrap">
                <div class="main">
                    <div class="near_footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="logo_footer col-xs-12 col-md-4 col-lg-4 ">
                                        <a href="/">
                                            <img width="210" alt="null"
                                                 src="<?php bloginfo('stylesheet_directory') ?>/images/logo.png"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="infor col-xs-12 col-md-6 col-lg-6">
                                        <?php
                                        query_posts("page_id=162");
                                        if (have_posts()):while (have_posts()):the_post();
                                            the_content();
                                        endwhile;endif;
                                        wp_reset_query();
                                        ?>
                                    </div>
                                    <div class="share col-xs-12 col-md-6 col-lg-6">
                                        <span class='st_facebook_large' displayText='Facebook'></span>
                                        <span class='st_twitter_large' displayText='Tweet'></span>
                                        <span class='st_delicious_large' displayText='Delicious'></span>
                                        <span class='st_pinterest_large' displayText='Pinterest'></span>
                                        <span class='st_email_large' displayText='Email'></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inner clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 ">
                                <div class="ja-box-center-c">
                                    <div class="moduletable" id="Mod22">
                                        <h3>Map</h3>
                                        <div class="ja-box-ct">

                                            <iframe class="embed-responsive-item-c"
                                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9525802985213!2d105.85166704251677!3d21.03458330043211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abbf53d1e70f%3A0xb05bd1fbf7de56a9!2zQ29uIELGsOG7m20gWGFuaA!5e0!3m2!1svi!2s!4v1468417679577"
                                                width="291" height="220" frameborder="0" style="border:0"
                                                allowfullscreen></iframe>
                                        </div>

                                    </div>

                                </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 ">
                                <div class="ja-box-center col-xs-12 col-md-6">
                                    <div class="moduletable" id="Mod43">
                                        <h3>Building</h3>
                                        <div class="ja-box-ct">
                                            <img  class="image-responsive" src="<?php bloginfo('stylesheet_directory') ?>/images/view.jpg">
                                        </div>
                                    </div>

                                </div>

                                <div class="ja-box-right col-xs-12 col-md-6" >
                                    <div id="TA_selfserveprop580" class="TA_selfserveprop">
                                        <ul id="xNffENFokT" class="TA_links fN8d95PP2">
                                            <li id="mkRy30qtsSy" class="3Ov1aP">
                                                <a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=580&amp;locationId=1463225&amp;lang=en_US&amp;rating=true&amp;nreviews=0&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=false&amp;display_version=2"></script>

                                </div>
                                </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/578f8dadfe074f3a5bfd1ec8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- Footer Ends -->
<?php
wp_footer(); // we need this for plugins
genesis_after();
?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<?php
if (is_user_logged_in()) {
?>
    <style>
        @media (max-width: 991px) and (min-width: 768px)
            .login-top {
                right: -32%;
            }
    </style>
<script>

    var contentWrapperHeight = jQuery('.content_wapper').height();
    /////////////////
    jQuery(function(){
        jQuery('#menu-nav-menu .menu-item-412').hide();
        jQuery('#menu-nav-menu .menu-item-258').hide();
        jQuery('#menu-nav-menu .menu-item-319').show();
        jQuery('#menu-nav-menu .menu-item-256').show();

    });

</script>
<?php }else{ ?>
    <script>
        jQuery(function(){

            jQuery('#menu-nav-menu .menu-item-412').show();
            jQuery('#menu-nav-menu .menu-item-258').show();
            jQuery('#menu-nav-menu .menu-item-319').hide();
            jQuery('#menu-nav-menu .menu-item-256').hide();
        });

</script>
<?php }?>
<script>
jQuery(function(){
    contentHeight();
    jQuery(window).resize(function() {
        contentHeight();
    });

    jQuery(window).load(function() {
        jQuery('#wpns_slider').nivoSlider({
            afterLoad: function(){
                contentHeight();
            } //Triggers when slider has loaded
        });
    });

});


    jQuery(function(){
        jQuery(function(){

            jQuery('#menu-nav-menu-1 .menu-item-412').hide();
            jQuery('#menu-nav-menu-1 .menu-item-258').hide();
            jQuery('#menu-nav-menu-1 .menu-item-319').hide();
            jQuery('#menu-nav-menu-1 .menu-item-256').hide();
        });
    });
jQuery(function(){

    Menu.init("menu-nav-menu-1", {"orientation": Menu.HORIZONTAL, "hidePause": 0.5});
});
    function contentHeight() {
//        jQuery('.content_wapper').css('height',jQuery('.parsys_column.booking-order').height());
        //jQuery('.content_wapper').height(contentWrapperHeight + jQuery('#wpns_slider').height());

    }
</script>
</body>
</html>