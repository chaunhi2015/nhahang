<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
		<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea">
					<div style="clear:both"></div>
					<div class="parsys_column">
						
							<?php
								query_posts(array('post_parent' => 4,"post_type"=>"page","posts_per_page"=>3));
								if(have_posts()):while(have_posts()):the_post();
							?>
								<div class="parsys_column promo-col-c0 item">
									<div class="parbase image section">
										<div id="">
											<a href="<?php the_permalink();?>">
												<?php the_post_thumbnail(array(315,250));?>
											</a>
										</div>					
									</div>
									<div class="titlecomponent section">
										<h2 class="titlenone"><?php the_title();?></h2>
									</div>
									<div class="richtext section">
										<div class="text">
											<?php echo substr(get_the_excerpt(), 0,90); ?>
										</div>
									</div>
									<div class="richtext section">
										<div class="textlink">
											<?php
												$data = get_post_meta( get_the_ID(), 'wpcf-choose-order', false );
												$foodtour = '';
												foreach($data as $val){															
													$foodtour = $val;
												}
												if($foodtour == 1){
											?>
											<a href="<?php echo bloginfo("url");?>/order-menu/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }else{ ?>
											<a href="<?php echo bloginfo("url");?>/foodtour-order/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }?>											
										</div>
									</div>
								</div>
							<?php endwhile;endif;wp_reset_query();?>
							<?php
								query_posts(array('post_parent' => 21,"post_type"=>"page","posts_per_page"=>3));
								if(have_posts()):while(have_posts()):the_post();
							?>
								<div class="parsys_column promo-col-c0 item">
									<div class="parbase image section">
										<div id="">
											<a href="<?php the_permalink();?>">
												<?php the_post_thumbnail(array(315,250));?>
											</a>
										</div>					
									</div>
									<div class="titlecomponent section">
										<h2 class="titlenone"><?php the_title();?></h2>
									</div>
									<div class="richtext section">
										<div class="text">
											<?php echo substr(get_the_excerpt(), 0,90); ?>
										</div>
									</div>
									<div class="richtext section">
										<div class="textlink">
											<?php
												$data = get_post_meta( get_the_ID(), 'wpcf-choose-order', false );
												$foodtour = '';
												foreach($data as $val){															
													$foodtour = $val;
												}
												if($foodtour == 1){
											?>
											<a href="<?php echo bloginfo("url");?>/order-menu/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }else{ ?>
											<a href="<?php echo bloginfo("url");?>/foodtour-order/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }?>
										</div>
									</div>
								</div>
							<?php endwhile;endif;wp_reset_query();?>
							<?php
								query_posts(array('post_parent' => 13,"post_type"=>"page","posts_per_page"=>3));
								if(have_posts()):while(have_posts()):the_post();
							?>
								<div class="parsys_column promo-col-c0 item">
									<div class="parbase image section">
										<div id="">
											<a href="<?php the_permalink();?>">
												<?php the_post_thumbnail(array(315,250));?>
											</a>
										</div>					
									</div>
									<div class="titlecomponent section">
										<h2 class="titlenone"><?php the_title();?></h2>
									</div>
									<div class="richtext section">
										<div class="text">
											<?php echo substr(get_the_excerpt(), 0,90); ?>
										</div>
									</div>
									<div class="richtext section">
										<div class="textlink">
											<?php
												$data = get_post_meta( get_the_ID(), 'wpcf-choose-order', false );
												$foodtour = '';
												foreach($data as $val){															
													$foodtour = $val;
												}
												if($foodtour == 1){
											?>
											<a href="<?php echo bloginfo("url");?>/order-menu/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }else{ ?>
											<a href="<?php echo bloginfo("url");?>/foodtour-order/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
												<?php }?>
										</div>
									</div>
								</div>
							<?php endwhile;endif;wp_reset_query();?>
							
					</div>
					<div style="clear:both"></div>
					<ul class="bxslider">
						<?php
							$args = array(
								'status' => 'approve',
								'number' => '4',
								'post_id' => 164, // use post_id, not post_ID
							);
							$comments = get_comments($args);
							foreach($comments as $comment){								
						?>
						<li class="testimonial-items bx-clone">
							<div class="item">
								<div class="picture">
									<a href="#" title="">
										<?php echo get_avatar( $comment, 120 ); ?>										
									</a>
								</div>								
								<div class="testimonial-details">
									<?php echo $comment->comment_content;?>
								</div>								
								<div class="guest-name">
									- <?php echo $comment->comment_author;?> -
								</div>
							</div>
						</li>
							<?php }?>
					</ul>
				</div>
			</div>
		<!--Content Area Ends -->
		
	
	<?php get_footer(); ?>