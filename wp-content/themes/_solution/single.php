<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea">
					<div style="clear:both"></div>
					<div class="parsys_column promo-col">
						<?php
							if(have_posts()):while(have_posts()):the_post();
							echo "<h2 class='title'>".get_the_title()."</h2>";
							the_content();						
							endwhile;endif;wp_reset_query();
						?>						
					</div>
					<div style="clear:both"></div>
					<div id="block-views-admin_article-block_2" class="block block-views tin-cung-loai">
						  <div class="gutter inner clearfix">
									<h2 class="title block-title">Tin cùng loại</h2>
									<div class="content clearfix">
							  <div class="view view-admin-article view-id-admin_article view-display-id-block_2 view-dom-id-d08648c2d880f6de9476ffff603d75ab">
								
						  
						  
							  <div class="view-content">
							  <div class="item-list">    
							  <ul>          
									<?php do_action(
									'related_posts_by_category',
									array(
									  'orderby' => 'post_date',
									  'order' => 'DESC',
									  'limit' => 5,
									  'echo' => true,
									  'before' => '<li>',
									  'outside' => '',
									  'after' => '</li>',
									  'rel' => 'nofollow',
									  'type' => 'post',				  
									  'message' => 'No matches'
									)
								  ) ?>
							  </ul></div>    
							  </div>
						  
						  
						  
						  
						  
						  
						</div>    </div>
							<div class="block-bg-bottom"></div>
						  </div><!-- /block-inner -->
					</div>
				</div>
			</div>
		<!--Content Area Ends -->
	<?php get_footer(); ?>