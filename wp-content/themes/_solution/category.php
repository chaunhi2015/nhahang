<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea">
					<div style="clear:both"></div>
					<div class="parsys_column news">
						<h4 class="title"><?php echo single_cat_title();?></h4>
						<div class="view-content">
							<?php							
								if(have_posts()):while(have_posts()):the_post();
							?>
							<div class="views-row views-row-1 views-row-odd views-row-first row-news">
						  
							  <div class="views-field views-field-field-image">        
								<div class="field-content">
									<a href="<?php the_permalink();?>">
										<?php the_post_thumbnail(array(150,150));?>
									</a>
								</div>  
							  </div>  
							  <div class="views-field views-field-title">
								<span class="field-content"><a href="<?php the_permalink();?>"><?php the_title();?></a></span>
							  </div>  
							  <div class="views-field views-field-field-lead">
								<div class="field-content">
									<?php the_excerpt();?>
								</div>  
							  </div>
							  <div class="views-field views-field-view-node">
								<span class="field-content"><a class="readmore" href="<?php the_permalink();?>">Chi tiết</a></span>
							  </div>
							  
							</div>
							<?php endwhile;endif;wp_reset_query();?>
					 
						</div>
					</div>
					<div style="clear:both"></div>
				</div>
			</div>
		<!--Content Area Ends -->
	<?php get_footer(); ?>