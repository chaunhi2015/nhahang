<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div class="master-wrapper-main">
            <div class="master-wrapper-content">
                


<div class="center-2">
    <div class="breadcrumb">
        <?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
    </div>
	<div class="page category-page">
		<div class="">
			Sorry about that. You can click <a href="<?php echo bloginfo("url");?>">here</a> to come back home and view tours another!			
		</div>
	</div>

    
</div>
<div class="side-2">

            <div>
<div id="TA_selfserveprop95" class="TA_selfserveprop"><div id="CDSWIDSSP" class="widSSP widSSPnarrow" style="width: 240px;"> <div class="widSSPData" style="border: 1px solid #589442;"> <div class="widSSPBranding"> <dl> <dt> <a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"></a> </dt> <dt class="widSSPTagline">Book your best trip, every trip</dt> </dl> </div><!--/ cdsBranding--> <div class="widSSPComponent"> <div class="widSSPSummary"> <dl> <a target="_blank" href="http://www.tripadvisor.com/Attraction_Review-g293924-d2234933-Reviews-Go_Asia_Travel_Day_Tours-Hanoi.html" onclick="ta.cds.handleTALink(11900,this);return true;" rel="nofollow"> <dt class="widSSPH18">Go Asia Travel - Day Tours</dt> </a> </dl> </div><!--/ cdsSummary--> </div><!--/ cdsComponent--> <div class="widSSPComponent widSSPOptional"> <div class="widSSPTrvlRtng"> <dl> <dt class="widSSPH11">TripAdvisor Traveler Rating</dt> <dd> <div class="widSSPOverall"> <img src="http://static.tacdn.com/img2/ratings/traveler/s4.5.gif" alt="4.5 of 5 stars" class="rsImg"> <div>Based on <b>309</b> traveler reviews</div> </div><!--/ overall --> </dd> </dl> </div> </div><!--/ cdsComponent --> <div class="widSSPWrap widSSPOptional"> <div class="widSSPInformation"> <div class="widSSPWrap"> <div class="widSSPPopIdx widSSPSingle"> <b>TripAdvisor Ranking</b> <span class="widSSPPopIdxData"> <span class="widSSPPopIdxData widSSPPopIdxNumbers"> <sup>#</sup>8 of 146 </span> Boat Tours &amp; Water Sports in Hanoi </span> </div><!--/ popIdx--> </div><!--/ cdsWrap--> </div><!--/ cdsInformation--> </div><!--/ cdsWrap--> <div class="widSSPComponent widSSPOptional"> <dl class="widSSPReviews"> <dt class="widSSPH11">Most Recent Traveler Reviews</dt> <dd class="widSSPOneReview"> <ul class="widSSPBullet"> <li> <span class="widSSPDate">Dec 10, 2015:</span> <span class="widSSPQuote">“Everything promised was delivered on 11 day...”</span> </li> <li> <span class="widSSPDate">Dec 10, 2015:</span> <span class="widSSPQuote">“Day trip to Hanoi”</span> </li> <li> <span class="widSSPDate">Dec 8, 2015:</span> <span class="widSSPQuote">“14 Day Best of Vietnam &amp; Cambodia...”</span> </li> <li> <span class="widSSPDate">Dec 6, 2015:</span> <span class="widSSPQuote">“Excelent tour guide”</span> </li> <li> <span class="widSSPDate">Dec 5, 2015:</span> <span class="widSSPQuote">“Great Guide and TRip”</span> </li> </ul><!--/ bullet--> </dd><!--/ hReview--> </dl> </div> <div class="widSSPAll"> <ul class="widSSPReadReview"> <li><a href="http://www.tripadvisor.com/Attraction_Review-g293924-d2234933-Reviews-Go_Asia_Travel_Day_Tours-Hanoi.html" id="allreviews" onclick="ta.cds.handleTALink(11900,this);window.open(this.href, 'newTAWindow', 'toolbar=1,resizable=1,menubar=1,location=1,status=1,scrollbars=1,width=800,height=600'); return false" rel="nofollow">Read reviews</a></li> </ul> <ul class="widSSPWriteReview"> <li><a href="https://www.tripadvisor.com/UserReview-g293924-d2234933-Go_Asia_Travel_Day_Tours-Hanoi.html" id="writereview" onclick="ta.cds.handleTALink(11900,this);window.open(this.href, 'newTAWindow', 'toolbar=1,resizable=1,menubar=1,location=1,status=1,scrollbars=1,width=800,height=600'); return false" rel="nofollow">Write a review</a></li> </ul> </div><!--/ cdsAll--> <div class="widSSPLegal">© 2015 TripAdvisor LLC</div><!--/ cdsLegal--> </div><!--/ cdsData--> </div><!--/ CDSPOP.cdsBx--> </div>

<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=95&amp;locationId=2234933&amp;lang=en_US&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;langversion=2"></script><script src="http://www.tripadvisor.com/WidgetEmbed-selfserveprop?border=true&amp;popIdx=true&amp;iswide=false&amp;locationId=2234933&amp;uniq=95&amp;rating=true&amp;lang=en_US&amp;nreviews=5&amp;writereviewlink=true&amp;langversion=2"></script>                
            </div>



   <?php
	genesis_after_content_sidebar_wrap();
   ?>            

</div>
            </div>
        
    </div>	
	<?php get_footer(); ?>