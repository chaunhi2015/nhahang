<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea">
					<div style="clear:both"></div>
					<div class="parsys_column promo-col order-menu">						
						<?php							
							if(have_posts()):while(have_posts()):the_post();
								echo "<h2 class='title'>".get_the_title()."</h2>";
								echo "<h3><b>".$_GET["n"]."</b></h3>";
								the_content();						
							endwhile;endif;wp_reset_query();
						?>						
					</div>
					<div style="clear:both"></div>
				</div>
			</div>
		<!--Content Area Ends -->
	<?php get_footer(); ?>