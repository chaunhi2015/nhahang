<?php
/**
 * Template Name: Booked
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 ?>
 
 <?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<!--Content Area starts -->
			<div class="content_box clearfix">
				<div class="parsys contentarea order">
					<?php
						if(have_posts()):while(have_posts()):the_post();
						$data = get_post_meta( get_the_ID(), 'wpcf-choose-order', false );
						$foodtour = '';
						foreach($data as $val){															
							$foodtour = $val;
						}
					?>
					<div class="parsys_column booking-order <?php if($foodtour == 2) echo "foodtour";?>">
						<div class="top_page"></div>
						<div class="bottom_page"></div>
						<div class="content_wapper">
							<div class="content">
								<?php							
									echo "<h2 class='title'>".get_the_title()."</h2>";
									the_content();
								?>
									<div class="texts">
										<?php
											$data = get_post_meta( get_the_ID(), 'wpcf-gia', false );								
											foreach($data as $val){															
												echo $val;
											}
										?>
									</div>
									<div class="price-text">
										PRICES ARE ALL INCLUSIVE
									</div>
									<?php								
										if($foodtour == 1){
									?>
									<a href="<?php echo bloginfo("url");?>/order-menu/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
										<?php }else{ ?>
									<a href="<?php echo bloginfo("url");?>/foodtour-order/?n=<?php echo get_the_title();?>" type="button" class="btn btn-outline btn-warning fl_right">Book Now</a>
										<?php }?>
								
								<p>
								<script type="text/javascript">
								jQuery(window).load(function() {
									jQuery('#wpns_slider').nivoSlider();
								});
								</script>

								<div style="width:400px;margin:auto">
									<div id="wpns_slider" class="nivoSlider">
										<?php
											$image_list = '';
											$attachment_ids = easy_image_gallery_get_image_ids();
											foreach ( $attachment_ids as $attachment_id ) {
												$image_link	= wp_get_attachment_image_src( $attachment_id, apply_filters( 'easy_image_gallery_linked_image_size', 'large' ) );
												$image	= $image_link[0];										
												$image_list .= '<a class="thumb-popup-link" href="#">';
													$image_list .= '<img src="'.$image.'" alt="'.get_the_title().'">';
												$image_list .= '</a>';
											}
																				
											echo $image_list;
										?>								
									</div>						
								</div>						
								<!--<img src="<?php echo bloginfo("stylesheet_directory");?>/images/img_slider.jpg"/>--> </p>
							</div>	
						</div>	
					</div>	
					<?php
						endwhile;endif;wp_reset_query();
					?>
				</div>
			</div>
		<!--Content Area Ends -->
	<?php get_footer(); ?>