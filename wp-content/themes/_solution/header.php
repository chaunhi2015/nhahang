<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 */
genesis_doctype();

genesis_meta();
?>
<meta content="INDEX,FOLLOW" name="robots" />
<?php
wp_head(); // we need this for plugins

genesis_title();

?>	
	<meta name="viewport" content="width=device-width, initial-scale=1">             
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/theme.css" type="text/css" media="screen" />
    <link href="<?php bloginfo('stylesheet_directory') ?>/style.css" rel="stylesheet"/>
	<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "52296926-f34e-48ca-a149-b6505aa6862c", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>
<body class="bodytag-fr">

<!--Header starts -->
<div class="head_foot_bg">
	<div class="global_width" id="header">
		<!--Header Logo Starts -->
		<div id="header_logo" class="clear fl">
			<div class="logo parsys iparsys"><div class="parbase headerlogo section">
				<div class="fl">
				<a href="/">
					<img class="headerLogoImage" alt="null" src="<?php bloginfo('stylesheet_directory') ?>/images/logo.png"/>
				</a>
				</div>
				<div class="clear"></div> </div>
				<div class="section"><div class="new"></div>
				</div>
				<div class="iparys_inherited">
					<div class="logo parsys iparsys">				
					</div>
				</div>
			</div>
			<ul class="login-top">
				<li><a href="<?php echo bloginfo("url")?>/login">
					<?php
						if ( is_user_logged_in() ) {							
							global $current_user;							
							echo 'Welcome, '.$current_user->user_login;
						} else {
							echo 'Login';
						}
					?>
				</a></li>
				<?php
						if ( is_user_logged_in() ) {
				?>
				<li>|</li>
				<li><a href="<?php echo bloginfo("url")?>/comment">Comment</a></li>
						<?php }?>
				<li>|</li>						
				<li><a href="<?php echo bloginfo("url")?>/register">Register</a></li>						
			</ul>
		</div>
		<!--Header Logo Ends -->
		
	</div>
</div>
<!--Header Ends -->
<div id="nav_contentarea">
		<div class="global_width" id="container">
		<!-- Mastead+reservation starts-->
			<div id="reserve_masthead" style="min-height:0px">
				<!-- Top-nav Starts -->
				<div class="topnav">
				<div id="nav" class="global_width content clearfix">
				<?php
					if ( genesis_get_option('nav_type') == 'nav-menu' && function_exists('wp_nav_menu') ) {
						
						$nav = wp_nav_menu(array(
							'theme_location' => 'primary',
							'container' => '',									
							'menu_class' => genesis_get_option('nav_superfish') ? 'menu' : 'top-menu',
							'echo' => 0
						));	
					} else {
						$nav = genesis_nav(array(
							'theme_location' => 'primary',
							'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish' : 'nav',
							'show_home' => genesis_get_option('nav_home'),
							'type' => genesis_get_option('nav_type'),
							'sort_column' => genesis_get_option('nav_pages_sort'),
							'orderby' => genesis_get_option('nav_categories_sort'),
							'depth' => genesis_get_option('nav_depth'),
							'exclude' => genesis_get_option('nav_exclude'),
							'include' => genesis_get_option('nav_include'),
							'echo' => false
						));
						
					}
					
					echo $nav;
				?>
				</div></div>
				<div class="heading_text">
				</div>
				<!-- Top-nav Ends -->

				<div class="hero">
				<div class="Masthead parsys"><div class="masthead section">
					<div id="mast_head1">
						<script type="text/javascript">
						jQuery(window).load(function() {
							jQuery('#bg_slider').nivoSlider();
						});
						</script>

						<div>
							<div id="bg_slider" class="nivoSlider">
								<?php
									query_posts("cat=10");
									if(have_posts()):while(have_posts()):the_post();									
										the_post_thumbnail("full");				
									endwhile;endif;wp_reset_query();
								?>							
							</div>						
						</div>
					</div>
				</div>
				</div>
				</div>
			</div>
		<!-- Mastead+reservation Ends-->