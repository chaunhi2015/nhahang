		</div>
	</div>
<!-- Footer Starts -->
	<div class="head_foot_bg">
		<div id="footer" class="global_width">
			<div class="clearfix" id="local_footer">
				<div id="ja-botsl1" class="wrap">
					<div class="main">
						<div class="near_footer">
							<div class="logo_footer">
								<a href="/">
									<img width="210" alt="null" src="<?php bloginfo('stylesheet_directory') ?>/images/logo.png"/>
								</a>
							</div>
							<div class="infor">
								<?php
									query_posts("page_id=162");
									if(have_posts()):while(have_posts()):the_post();
										the_content();
									endwhile;endif;wp_reset_query();
								?>
							</div>
							<div class="share">
								<span class='st_facebook_large' displayText='Facebook'></span>
								<span class='st_twitter_large' displayText='Tweet'></span>
								<span class='st_delicious_large' displayText='Delicious'></span>
								<span class='st_pinterest_large' displayText='Pinterest'></span>
								<span class='st_email_large' displayText='Email'></span>
							</div>
						</div>
						<div class="inner clearfix">						
							<div class="ja-box-left" style="width: 33.17%;">
									<div class="moduletable" id="Mod22">
											<h3>Map</h3>
											<div class="ja-box-ct">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9525802985213!2d105.85166704251677!3d21.03458330043211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abbf53d1e70f%3A0xb05bd1fbf7de56a9!2zQ29uIELGsOG7m20gWGFuaA!5e0!3m2!1svi!2s!4v1468417679577" width="291" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>											
										</div>
					   
									</div>
						
						  </div>
							  
							  <div class="ja-box-center" style="width: 33.17%;">
										<div class="moduletable" id="Mod43">
											<h3>Building</h3>
											<div class="ja-box-ct">
											<img width="291" src="<?php bloginfo('stylesheet_directory') ?>/images/view.jpg">	
										</div>
									</div>
						
							</div>
							
							<div class="ja-box-right" style="width: 33%;float:right">
								<div id="TA_selfserveprop539" class="TA_selfserveprop">
									<ul id="FeSqvf" class="TA_links 0L0y9570o">
									<li id="jyEmjP" class="A4XT6LaC3F">
									<a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
									</li>
									</ul>
									</div>
									<script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=539&amp;locationId=1463225&amp;lang=en_US&amp;rating=true&amp;nreviews=1&amp;writereviewlink=true&amp;popIdx=false&amp;iswide=false&amp;border=false&amp;display_version=2"></script>
								</div>						  
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Footer Ends -->
<?php
	wp_footer(); // we need this for plugins
	genesis_after();
?>
</body>
</html>