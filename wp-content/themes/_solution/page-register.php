<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

<?php genesis_before_content(); ?>
    <!--Content Area starts -->
    <div class="container register" >
        <div class="content_box clearfix">
            <div class="parsys contentarea">
                <div style="clear:both"></div>
                <div class="parsys_column ca form-register col-md-9 col-md-push-2">
                    <?php
                    global $post;
                    $post_slug = $post->post_name;
                    if (have_posts()):while (have_posts()):the_post();
                        echo "<h2 class='text-center title'>" . get_the_title() . "</h2>";
                        the_content();
                    endwhile;endif;
                    wp_reset_query();
                    ?>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <!--Content Area Ends -->
<?php get_footer(); ?>